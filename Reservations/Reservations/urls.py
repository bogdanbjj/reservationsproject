from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r"reservation/", include("reservation.urls", namespace='reservation')),
    url(r'^$', view=TemplateView.as_view(template_name='index.html'), name='index'),
)
