from django.db import models


class Reservation(models.Model):
    famName = models.CharField(max_length=30, blank=False, null=False)
    arrivalDate = models.DateField(blank=False, null=False)
    departureDate = models.DateField(blank=False, null=False)
    room = models.PositiveSmallIntegerField(null=False, blank=False)

    class Meta:
        unique_together = ("room", 'famName',)

    def __unicode__(self):
        return "{}, {}".format(self.famName, self.room)