from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from .forms import CreateReservationForm, DeleteReservationForm, FindReservationForm
from .models import Reservation


def create_view(request):
    form = CreateReservationForm(request.POST or None)
    if form.is_valid():
        name = form.cleaned_data['famName']
        arr = form.cleaned_data['arrivalDate']
        dep = form.cleaned_data['departureDate']
        room = form.cleaned_data['room']
        r = Reservation(famName=name, arrivalDate=arr, departureDate=dep, room=room)
        r.save()
        return HttpResponseRedirect(reverse('index'))
    return render(request, "reservation/create_reservation.html", {'form': form})


def delete_view(request):
    form = DeleteReservationForm(request.POST or None)
    if form.is_valid():
        return HttpResponseRedirect(reverse('index'))
    return render(request, 'reservation/delete_reservation.html', {'form': form})


def search_view(request):
    """this view is responsible for finding a reservation
    by name and room"""
    form = FindReservationForm(request.POST or None)
    if form.is_valid():
        name = form.cleaned_data.get('famName')
        room = form.cleaned_data.get('room')
        try:
            if name and room:
                r = Reservation.objects.get(famName=name, room=room)

            return redirect(reverse("reservation:show_result", args=[r.id]))
        except Reservation.DoesNotExist:
            return render(request, 'reservation/show_reservation.html', {})

    return render(request, 'reservation/find_reservation.html', {'form': form})


def show_view(request, r_id):
    r = Reservation.objects.get(pk=r_id)
    return render(request, 'reservation/show_reservation.html', {'r': r})