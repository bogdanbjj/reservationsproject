from datetime import date

from django import forms

from .models import Reservation


class ReservationForm(forms.Form):
    famName = forms.CharField(max_length=30, label='Familly name')
    arrivalDate = forms.DateField(label='Arrival date', input_formats=['%d-%m-%Y', '%d/%m/%Y', '%d/%m/%y'],
                                  widget=forms.TextInput(attrs={'placeholder': 'ex: 10-05-2016'}))

    departureDate = forms.DateField(label='Departure date', input_formats=['%d-%m-%Y', '%d/%m/%Y', '%d/%m/%y'],
                                    widget=forms.TextInput(attrs={'placeholder': 'ex: 25-05-2016'}))
    room = forms.IntegerField(label="Room number")


class CreateReservationForm(ReservationForm):
    def __init__(self, *args, **kwargs):
        super(CreateReservationForm, self).__init__(*args, **kwargs)


    def error_msg(self):
        raise forms.ValidationError("There is a reservation in that room in that timeframe")

    def clean(self):
        """clean method gets run once for every field"""
        cleaned_data = self.cleaned_data
        date1 = cleaned_data.get('arrivalDate')
        date2 = cleaned_data.get('departureDate')
        room = cleaned_data.get('room')
        name = cleaned_data.get('famName')

        if date1 and date1 < date.today():
            raise forms.ValidationError("You can't make reservations that are in the past")

        if (date1 and date2) and date1 >= date2:
            raise forms.ValidationError("Departure date must be greater than arrival date")

        # check if name and room are unique
        r = Reservation.objects.filter(famName=name, room=room)
        # if it exists
        if r:
            raise forms.ValidationError("There can only be one reservation per room. Try "
                                        "a different room")

        # different check than the one for uniqueness
        # dont filter by name, because name might not corespond to what's in the db
        reservation = Reservation.objects.filter(room=room)

            # if reservations with room number x exist
            # check their dates to see if they overlap
            # if they overlap raise ValidationError
        if reservation:
            for r in reservation:
                if r.arrivalDate <= date1 < r.departureDate:
                    self.error_msg()
                if r.arrivalDate < date2 <= r.departureDate:
                    self.error_msg()
                if date1 < (r.arrivalDate and r.departureDate) and date2 > (r.arrivalDate and r.departureDate):
                    self.error_msg()

        return cleaned_data


class IntermediaryReservationForm(ReservationForm):
    def __init__(self, *args, **kwargs):
        super(IntermediaryReservationForm, self).__init__(*args, **kwargs)
        del self.fields['arrivalDate']
        del self.fields['departureDate']


class DeleteReservationForm(IntermediaryReservationForm):
    def __init__(self, *args, **kwargs):
        super(DeleteReservationForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('famName')
        room = cleaned_data.get('room')

        if name and room:
            try:
                Reservation.objects.get(famName=name, room=room).delete()
            except Reservation.DoesNotExist:
                raise forms.ValidationError("There is no reservation for {} in room {}".format(name, room))

        return cleaned_data


class FindReservationForm(IntermediaryReservationForm):
    pass