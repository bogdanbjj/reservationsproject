from django.conf.urls import url, patterns

from .views import create_view, delete_view, search_view, show_view

urlpatterns = patterns('',
    url(r'^create/$', view=create_view, name='create_query'),
    url(r'^delete/$', view=delete_view, name='delete_query'),
    url(r'^search/$', view=search_view, name='search_query'),
    url(r'^show_result/(?P<r_id>\d+)/$', view=show_view, name='show_result'),
)