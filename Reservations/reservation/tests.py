from datetime import date

from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.forms import forms
from django.test import TestCase

from .forms import CreateReservationForm, DeleteReservationForm
from .models import Reservation


class TestCreateReservationForm(TestCase):
    def setUp(self):
        Reservation.objects.create(famName="Vasilescu", arrivalDate=date(2019,6,1),
                                            departureDate=date(2016,6,5), room=3)
        Reservation.objects.create(famName="Vasilescu", arrivalDate=date(2016,6,6),
                                             departureDate=date(2019,6,15), room=4)


    def test_empty_form(self):
        """form should not be valid"""
        form_data = {'famName': '', 'arrivalDate': '',
                    'departureDate': '', 'room': ''}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_valid_form(self):
        """form should be valid"""
        form_data = {'famName': 'Pop', 'arrivalDate': date(2019, 6,29),
                     'departureDate': date(2019, 6, 30), 'room': 3}
        form = CreateReservationForm(data=form_data)
        self.assertTrue(form.is_valid())


    """make sure the validation rules stand"""
    def test_date1_in_the_past(self):
        """form should not be valid and should fail with specific error"""
        form_data = {'famName':'Pop', 'arrivalDate':date(2016,5,14),
                     'departureDate': date(2016,5,15), 'room': 3}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'], ["You can't make reservations that are in the past"])


    def test_date1_in_the_present(self):
        """form validation should pass"""
        form_data = {'famName': 'Pop', 'arrivalDate': date(2019, 5, 22),
                    'departureDate': date(2019, 5, 23), 'room': 3}
        form = CreateReservationForm(data=form_data)
        self.assertTrue(form.is_valid())


    def test_date2_smaller_date1(self):
        """form should not be valid and should fail with specific error"""
        form_data = {'famName': 'Pop', 'arrivalDate': date(2016, 5, 26),
                    'departureDate': date(2016, 5, 23), 'room': 3}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'], ["Departure date must be greater than arrival date"])

        # response = self.client.post('/reservation/create_query/', {'famName':'Pop', 'arrivalDate':date(2016,5,14),
        #                             'departureDate': date(2016,5,15), 'room': 3})
        # self.assertFormError(response, , 'arrivalDate', "You can't make reservations that are in the past")

    def test_with_existent_reservation1(self):
        """form should fail beacause there is
        a reservation in room 3 during that time.
        Test checks the before and during interval
        Ex: 0 = existent reservation
            1 = arrival date
            test checks this interval 0 - 1 - 0"""
        # first try
        form_data = {'famName': 'Vasilescu', 'arrivalDate': date(2019, 5, 26),
                     'departureDate': date(2019, 6, 3), 'room': 3}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())

        # second try
        form_data = {'famName': 'Vasilescu', 'arrivalDate': date(2016, 6, 1),
                    'departureDate': date(2016, 6, 5), 'room': 4}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_with_existent_reservation2(self):
        """form should fail beacause there is
        a reservation in room 3 during that time.
        Test checks the during and after interval
        Ex: 0 = existent reservation
            2 = departure date
            test checks this interval 0 - 2 - 0
        """
        # first try
        form_data = {'famName': 'Pop', 'arrivalDate': date(2016, 6, 4),
                     'departureDate': date(2016, 6, 9), 'room': 4}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())

        # second try
        form_data = {'famName': 'Pop', 'arrivalDate': date(2016, 6, 12),
                     'departureDate': date(2016, 6, 17), 'room': 4}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())


    def test_with_existent_reservation3(self):
        """form should fail beacause there is
        a reservation in room 3 during that time.
        Test checks the before and after interval
        with reservation in the middle.
        Ex: 0 = existent reservation
            1 = arrival date
            2 = departure date
            test checks this interval 1 - 0 - 0 - 2"""
        form_data = {'famName': 'Pop', 'arrivalDate': date(2016, 5, 12),
                     'departureDate': date(2016, 6, 17), 'room': 3}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())

        form_data = {'famName': 'Pop', 'arrivalDate': date(2016, 3, 4),
                     'departureDate': date(2016, 9, 3), 'room': 3}
        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_form_error_message(self):
        form_data = {'famName': 'Pop', 'arrivalDate': date(2016, 5, 12),
                     'departureDate': date(2016, 6, 17), 'room': 3}
        form = CreateReservationForm(data=form_data)

        with self.assertRaises(ValidationError):
            form.error_msg()

    def test_form_condition_unique_together(self):
        """test should fail with specified error message"""
        form_data = {'famName': 'Vasilescu', 'arrivalDate': date(2016, 5, 26),
                    'departureDate': date(2016, 5, 29), 'room': 3}

        form = CreateReservationForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'], ["There can only be one reservation per room. "
                                                  "Try a different room"])


class TestDeleteReservationForm(TestCase):
    def setUp(self):
        Reservation.objects.create(famName="Vasilescu", arrivalDate=date(2019, 3,4),
                                   departureDate=date(2019,4,4), room=4)

    def test_post_blank_data(self):
        """form should not be valid. there should be
        errors in both fields"""

        form_data = {'famName': '', 'room': ''}
        form = DeleteReservationForm(data=form_data)
        self.assertFalse(form.is_valid())
        # self.assertEqual(form.errors['__all__'], ['This field is required.'])
        self.assertEqual(form.errors['famName'], ['This field is required.'])
        self.assertEqual(form.errors['room'], ['This field is required.'])

    def test_incomplete_data(self):
        """form should not be valid. There should be form error
        in room field"""
        form_data = {'famName': 'Pop', 'room': ''}
        form = DeleteReservationForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['room'], ['This field is required.'])

    def test_incomplete_data2(self):
        """form should not be valid. There should be form error
            in name field"""
        form_data = {'famName': '', 'room': 3}
        form = DeleteReservationForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['famName'], ["This field is required."])

    def test_delete_inexistent_data(self):
        """form should not be valid and should fail with specific
        error message"""
        form_data = {'famName': 'Vasilescu', 'room': 3}
        form = DeleteReservationForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'], ["There is no reservation for Vasilescu in room 3"])

    def test_valid_form(self):
        form_data = {'famName': 'Vasilescu', 'room': 4}
        form = DeleteReservationForm(data=form_data)
        self.assertTrue(form.is_valid())



class TestModels(TestCase):
    def test_unique_together_constraint(self):
        # should not be able to create 2 reservations
        # with same name in the same room
        with self.assertRaises(IntegrityError):
            Reservation.objects.create(famName="Vasilescu", arrivalDate=date(2016,6,1),
                                            departureDate=date(2016,6,5), room=17)
            Reservation.objects.create(famName="Vasilescu", arrivalDate=date(2016, 6, 8),
                                       departureDate=date(2016, 6, 13), room=17)

    def test_correct(self):
        # no errors should occur
        Reservation.objects.create(famName="Vasilescu", arrivalDate=date(2016, 6, 1),
                                   departureDate=date(2016, 6, 5), room=16)
        Reservation.objects.create(famName="Vasilesc", arrivalDate=date(2016, 6, 1),
                                   departureDate=date(2016, 6, 5), room=16)


class TestViews(TestCase):
    def test_response_is_ok(self):
        """status codes should be ok"""
        url = '/reservation/create/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_post_blank_data(self):
        """should raise formerror"""
        url = '/reservation/create/'
        response = self.client.post(url, data={'famName': '',
                                               'arrivalDate': '',
                                               'departureDate': '',
                                               'room': ''})
        # self.assertRedirects(response, '/reservation/')
        self.assertFormError(response, 'form', 'famName', 'This field is required.')
        self.assertFormError(response, 'form', 'arrivalDate', 'This field is required.')
        self.assertFormError(response, 'form', 'departureDate', 'This field is required.')
        self.assertFormError(response, 'form', 'room', 'This field is required.')


    def test_successfull_post(self):
        data = {'famName':"Vasilescu", 'arrivalDate': date(2016,6,1),
                'departureDate': date(2016,6,5), 'room': 15}

        url = '/reservation/create/'
        response = self.client.post(url, data, follow=True)
        # self.assertRedirects(response, '/')
        # self.assertEqual(response.status_code, 302)
